#include<iostream>
#include<string>

#include"Location.h"

using namespace std;

Location::Location() {}

void Location::setLocation(string name, int xstart, int xend, int ystart, int yend) {
	
	for (int i = xstart; i <= xend; i++) {
		for (int j = ystart; j <= yend; j++) {
			coord[i][j] = name;
		}
	}
}

void Location::createMap() {
	setLocation("Pallet Town", 0, 3, 0, 3);
	setLocation("Viridian City", 3, 6, 0, 3);
	setLocation("Pewter City", 6, 9, 0, 3);
	setLocation("Route 1", 0, 3, 3, 6);
	setLocation("Route 2", 3, 6, 3, 6);
	setLocation("Route 24", 6, 9, 3, 6);
}