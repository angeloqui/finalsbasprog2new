#include<iostream>
#include<string>

#include"Settings.h"

using namespace std;

Settings::Settings() {
	
	for (int i = 0; i < 2; i++) {
		system("cls");
		displayAsciiArt();
	
		if (i == 0)
			isVisible = inputSettings(isVisible, "\nShow Flavor Text? \nDisplays the intro quote of the game."); 
		else
			hasNPC = inputSettings(hasNPC, "\nHave NPCs? \nAdds a rival to the game to earn money to buy pokeballs");	
	}
}

int Settings::inputSettings(bool b, string str) {
		
	cout << "\n" << string(20, '=') << " Settings " << string(20, '=');
	cout << str;	
	cout << "\n\n[0] NO \n[1] YES \nINPUT: ";
	cin >> b;
	
	return b;
}

void Settings::displayAsciiArt() {
	printf("     ____       _                               	 \n");
	printf("    |  _ \\ ___ | | _____ _ __ ___   ___  _ __   	 \n");
	printf("    | |_) / _ \\| |/ / _ \\ '_ ` _ \\ / _ \\| '_ \\  \n");
	printf("    |  __/ (_) |   <  __/ | | | | | (_) | | | | 	 \n");
	printf("  __|_| _ \\___/|_|\\_\\___|_| |_| |_|\\___/|_| |_|  \n");
	printf(" / ___|(_)_ __ ___  _   _| | __ _| |_ ___  _ __ 	 \n");
	printf(" \\___ \\| | '_ ` _ \\| | | | |/ _` | __/ _ \\| '__| \n");
	printf("  ___) | | | | | | | |_| | | (_| | || (_) | |   	 \n");
	printf(" |____/|_|_| |_| |_|\\__,_|_|\\__,_|\\__\\___/|_|    \n");
}
