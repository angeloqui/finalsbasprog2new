#include<iostream>
#include<string>

//rand
#include<stdlib.h>
#include<time.h> 

//getch
#include<conio.h>

//local lib
#include"Settings.h"
#include"Dialogue.h"
#include"Player.h"
#include"Pokemon.h"
#include"Location.h"

using namespace std;

int main() {
			
	srand (time(NULL));
	
	Settings* settings = new Settings();
	
	// Initialization (and a lot of flavor text)
	Dialogue* dialogue = new Dialogue();
	
	dialogue->displayFlavorText(settings->isVisible, 0, 9);
	dialogue->displayFlavorText(settings->isVisible, 10, "\n\n[1] BOY \n[2] GIRL \n\nINPUT: ");
	
	Player* player[2];
	
	system("cls");
	
	player[0] = new Player();
	player[0]->getPlayerName(dialogue->flavor[11], "\n\nINPUT: ");
	
	if (settings->hasNPC == true) {
		
		dialogue->displayFlavorText(settings->isVisible, 13, 14);
		
		system("cls");
		
		player[1] = new Player();
		player[1]->getPlayerNamePreset(dialogue->flavor[15], "\n\n[0] NEW NAME \n[1] GREEN \n[2] GARY \n[3] KAZ \n[4] TORU \n\nINPUT: ");

		dialogue->displayFlavorText(settings->isVisible, dialogue->flavor[16], player[1]->name, "!");
	}
	
	dialogue->displayFlavorText(settings->isVisible, "" , player[0]->name, "!");
	dialogue->displayFlavorText(settings->isVisible, 17, 18);
	
	Location* location = new Location();
	location->createMap();
	
	// Game
	int panel = 0;
	
	Pokemon* encounteredPokemon = new Pokemon();

	system("cls");
	
	player[0]->selectStarter();
	player[0]->pokemon->getPokemonList(player[0]->pokemon, player[0]->select - 1);
	player[0]->pokemon->next = player[0]->pokemon;
	player[0]->pokemon->level = 2;
	player[0]->pokemon->scaleStats(player[0]->pokemon->level);
	
	if (settings->hasNPC == true) {
		if (player[0]->select == 1) 
			player[1]->select = 2;
		else if (player[0]->select == 2) 
			player[1]->select = 0;
		else if (player[0]->select == 3) 
			player[1]->select = 1;
			
		player[1]->pokemon->getPokemonList(player[1]->pokemon, player[1]->select);
	}
	
	// Game Loop
	while (true) {
		
		system("cls");
		
		// UI Setup
		player[0]->location = location->coord[player[0]->x][player[0]->y];
		if (player[0]->location == "Pallet Town" || player[0]->location == "Viridian City" || player[0]->location == "Pewter City") 
			player[0]->inTown = 1;
		else 
			player[0]->inTown = 0;
		player[0]->showUserInterface();
		
		// Main
		if (panel == 0) {
			
			if (player[0]->isfainted == true) {
				player[0]->isfainted = false;
				player[0]->pokemon->healPokemons(player[0]->pokemon);
			}
			
			player[0]->Main(panel, settings->hasNPC, player[1]);
			
			if (player[0]->inCombat == true) {
				encounteredPokemon = player[1]->pokemon;
				encounteredPokemon->level = player[0]->npcLevel[player[0]->npcBattleCount];
				encounteredPokemon->scaleStats(encounteredPokemon->level);
				
				player[0]->isNpcBattle = true;
			}
		}
		
		// Move
		else if (panel == 1) {
			
			player[0]->Move(panel);
			
			//If encountered a wild Pokemon
			if (player[0]->inCombat == true) {
				encounteredPokemon->getPokemonList(encounteredPokemon, rand() % 15);
				encounteredPokemon->level = rand() % 10 + 1;
				encounteredPokemon->scaleStats(encounteredPokemon->level);
						
				panel = 5;
			}
		}
			
		// Pokemon Data
		else if (panel == 2) {
			player[0]->Data(panel);
		}
		
		// POKeMON Center
		else if (panel == 3) {
			player[0]->Heal(panel);
		}
		
		// Poke Mart
		else if (panel == 4) {
			player[0]->Shop(panel);
		} 

		//Pokemon Battle
		else if (panel == 5) {	
			player[0]->Battle(panel, encounteredPokemon);
		}
	}
	
	//Cleanup
	delete settings;
	delete dialogue;
	delete[] player;
	delete location;
	
	return 0;
}
