#pragma once
#include<string>

using namespace std;

class Location {
	
public:
	string coord[10][10];
	
	Location();
	void setLocation(string name, int xstart, int xend, int ystart, int yend);
	void createMap();
};
